https://www.uniprot.org/help/proteome

(A) uniprot-proteome_UP000005640.fasta
79038 proteins from the Reference proteome.

(B) UP000005640_9606.fasta
20588 proteins (one protein sequence per gene)

(C) uniprot-proteome_UP000005640+reviewed_yes.fasta
20360 proteins (only reviewed entries)
