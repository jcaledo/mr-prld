# README #

This repo contains code and data related to the paper "A census of human methionine-rich prion-like domain containing proteins"
(Antioxidants 2022, 11(7), 1289; https://doi.org/10.3390/antiox11071289)